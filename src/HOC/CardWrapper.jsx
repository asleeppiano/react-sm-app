import React, {Component} from "react";

const CardWrapper = (WrappedComponent) => {
  return class extends Component {
    constructor(props){
      super(props)
      this.state = {
        opts:{
          title: {
            display: true,
            text: props.header,
          },
          scales: {
            xAxes: [{
              type: 'realtime',
              realtime: {
                refresh: 1000,
                delay: 1000,
                duration: 5000,
              }
            }],
            yAxes: [{
              scaleTime: {
                display: true,
              }
            }]
          },
          tooltips: {
            mode: 'nearest',
            intersect: false
          },
          hover: {
            mode: 'nearest',
            intersect: false
          },
          plugins: {
            streaming: {
              frameRate: 30,
            },
          },
        }
      }
    }
    render(){
      return <WrappedComponent wrappedstate={this.state} {...this.props}/>
    }
  }
}

export default CardWrapper
