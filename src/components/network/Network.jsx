import React, {Component} from "react";
import style from "./Network.module.css";
import Table from "../table/Table.jsx"
import Loading from "../loading/Loading"
import {AppContext} from '../../context/app-context'

class Network extends Component{

  constructor(props){
    super(props)
    this.state = {
      connstat: [],
    }
  }

  componentDidMount(){
    fetch('/api/net/connstat?' + this.context.guest)
      .then((res) => 
        res.json()
      )
      .then((myJson) => {
        this.setState({connstat: myJson})
      })
      .catch((err) => {
        console.log(err)
      })
  }
  render(){
    let table = <Loading />
    if (this.state.connstat.length !== 0){
      table = <Table caption="network connections" data={this.state.connstat}/>
    }
    return(
      <div className={style.Network}>
        {table}    
      </div>
    );
  }
}
Network.contextType = AppContext

export default Network;
