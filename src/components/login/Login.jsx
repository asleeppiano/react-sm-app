import React, {Component} from "react";
import style from "./Login.module.css";
import * as Auth from '../utils/Auth'
import { BrowserRouter as Router, Redirect, Link } from 'react-router-dom'
import {AppContext} from '../../context/app-context'
import Error from "../error/Error"

class Login extends Component{
  constructor(props){
    super(props)
    this.asGuest = this.asGuest.bind(this)
    this.login = this.login.bind(this)
    this.onChange = this.onChange.bind(this)
    this.changeTitle = this.changeTitle.bind(this)
    this.state = {
      title: 'Login',
      name: '',
      password: '',
      confirmPassword: '',
      error: '',
      isLoading: false,
      redirectToReferrer: false,
    }
  }
  async login(e){
    e.preventDefault()
    if(this.state.title === 'Login'){
      let res = await Auth.login(this.state.name, this.state.password)
      console.log('login res', res)
      if(Auth.isAuthenticated()){
        this.props.loadLayout()
        this.context.setUser(Auth.getUser())
        this.setState({redirectToReferrer: true, error: false}) 
        return
      }
      this.setState({error: true})
      return
    }
    if(this.state.password === this.state.confirmPassword){
      let res = await Auth.register(this.state.name, this.state.password)
      console.log('register res', res)
      if(Auth.isAuthenticated()){
        this.props.loadLayout()
        this.context.setUser(Auth.getUser())
        this.setState({redirectToReferrer: true, error: false}) 
        return
      }
      this.setState({error: true})
    }
  }
  onChange(e){
    this.setState({[e.target.name]: e.target.value})
  }
  changeTitle(){
    this.setState((prevState)=>{
      return {title: prevState.title === 'Login' ? 'Register' : 'Login'} 
    })
  }
  asGuest(){
    Auth.loginAsGuest()
    if(Auth.isAuthenticated()){
      this.props.loadLayout()
      this.context.setUser(Auth.getUser())
      this.context.guest = "guest=true"
      this.setState({redirectToReferrer: true}) 
      return
    }
  }
  render(){
    let {from} = this.props.location.state || {from: {pathname: '/'}}
    if(this.state.redirectToReferrer) return <Redirect to={from}/>
    let confirmPassword = ''
    let changeTitleButton = (
      <span className={style.ButtonChangeTitle} onClick={this.changeTitle}>
        want to <span style={{color: 'orange'}}>register</span> ?
      </span>
    )
    let errorMessage = ''
    if(this.state.error){
      errorMessage = <Error message={this.state.error ? "cannot login": "cannot register"}/>
    }
    if(this.state.title === 'Register'){
      confirmPassword = (
        <div>
          <label htmlFor='confirmPassword'>confirm password</label>
          <input id='confirmPassword' onChange={this.onChange} name='confirmPassword' minLength={6} required type='password'/>
        </div>
      )
      changeTitleButton = (
        <span className={style.ButtonChangeTitle} onClick={this.changeTitle}>
          already <span style={{color:'orange'}}>registered</span> ?
        </span>
      )
    }
    return(
      <div className={style.Login}>
        <h1> DASHBOARD </h1>
        <div className={style.FormWrapper}>
        <form className={style.SubmitForm} onSubmit={this.login}>
          <h1> {this.state.title} </h1>
          <div>
            <label htmlFor='name'>name</label>
            <input id='name' name='name' required onChange={this.onChange} type='text'/>
          </div>
          <div>
            <label htmlFor='password'>password</label>
            <input id='password' onChange={this.onChange} name='password' minLength={6}  required type='password'/>
          </div>
          {confirmPassword}
          <div>
            <label htmlFor='submitButton'>submit</label>
            <button id='submitButton' className={style.SubmitButton} name='submitButton' type='submit'>submit</button>
          </div>
        </form>
        {errorMessage}
          {changeTitleButton}
          <span className={style.ButtonChangeTitle} onClick={this.asGuest}> login as <span style={{color: 'orange'}}>guest</span> ? </span>
        </div>
      </div>
    );
  }
}
Login.contextType = AppContext
export default Login;
