import React, {Component} from "react";
import style from "./Layout.module.css";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import User from '../user/User'

class Layout extends Component{
  constructor(props){
    super(props)
    this.setSectonName = this.setSectonName.bind(this)
    this.showMenu = this.showMenu.bind(this)
    this.state = {
      preventClick: false,
      showMenu: false,
      divStyle: style.RemoveLayout,
      sectionName: document.location.pathname === '/' ? 
      'system overview' : 
      document.location.pathname.match(/\w+/),
    }
  }
  componentDidUpdate(prevProps){
    if(prevProps.load !== this.props.load){
      this.setState({divStyle: style.Layout})
    }
  }
  setSectonName(e){
    e.persist()
    console.log('setSectonName', e.target)
    if(e.target.innerText === undefined || e.target.localName !== 'a' || e.target.pathname === '/profile'){
      let el = e.target
      while(el.localName !== 'a'){
        el = el.parentElement
      }
      const pathname = el.href.split('/')
      this.setState((prevState) => {return{sectionName: pathname[pathname.length - 1]}})
      return
    }
    this.setState((prevState)=>{return{sectionName: e.target.innerText,}}, ()=>{
      if(window.innerWidth < 1000){
        this.setState((prevState)=>{return{showMenu: prevState.showMenu ? false: true,
          preventClick: prevState.preventClick ? false : true}})
        document.body.classList.toggle(style.StopScrolling)
      }
    })
  }
  showMenu(e){
    console.log('show menu', e.target)
    if(window.innerWidth < 1000){
      this.setState((prevState)=> {return {showMenu: prevState.showMenu === true ? false : true,
        preventClick: prevState.preventClick ? false : true}}, ()=>{
          document.body.classList.toggle(style.StopScrolling)
        })
    }
  }
  render(){
    let DashboardMenuStyle = ''
    if (this.state.showMenu === true){
      DashboardMenuStyle = style.DashboardMenuShow
    }
    return(
      <React.Fragment>
        <div className={this.state.divStyle}>
          <aside className={`${style.DashboardMenu} ${DashboardMenuStyle}`}>
            <div className={style.DashboardHeader}>
              <h1 onClick={this.state.showMenu ? this.showMenu : undefined}> System monitor </h1>
            </div>
            <div className={style.ButtonLayout}>
              <NavLink
                onClick={this.setSectonName}
                activeClassName={style.ButtonEnabled}
                exact to="/"
                className={style.DashboardButton}>
                system overview
              </NavLink>
              <NavLink
                onClick={this.setSectonName}
                activeClassName={style.ButtonEnabled}
                to="/network/"
                className={style.DashboardButton}>
                network
              </NavLink>
              <NavLink
                onClick={this.setSectonName}
                activeClassName={style.ButtonEnabled}
                to="/disks/"
                className={style.DashboardButton}>
                disks
              </NavLink>
              <NavLink
                onClick={this.setSectonName}
                activeClassName={style.ButtonEnabled}
                to="/processes/"
                className={style.DashboardButton}>
                processes
              </NavLink>
            </div>
          </aside>
          <div onClick={this.state.showMenu ? this.showMenu : undefined} className={style.ContentLayout}>
            <header className={style.Header}>
              <div className={style.HeaderNameMenuContainer}>
                <i onClick={this.showMenu} className={`${style.MenuButton} material-icons`}>menu</i>
                <h3> {this.state.sectionName} </h3>
              </div>
              <User preventClick={this.state.preventClick} setSectionName={this.setSectonName}/>
            </header>
            <main className={style.MainSection}>
              {this.props.children}
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Layout;
