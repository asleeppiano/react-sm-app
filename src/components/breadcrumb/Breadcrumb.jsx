import React, {Component} from "react";
import style from "./Breadcrumb.module.css";

class Breadcrumb extends Component{
  listItemHandler(e){
    if (e.target !== undefined){
      this.props.onPathChange(e.currentTarget.getAttribute('value').split(','))
    }
  }
  render(){
    return(
      <div className={style.Breadcrumb}>
        <ul className={style.BreadList}>
        {this.props.path.map((data, index, array) => {
          return (
            <li value={array.slice(0, index+1)} onClick={this.listItemHandler.bind(this)} key={index}><span>{data}</span></li>
        )})} 
        </ul>
      </div>
    );
  }
}

export default Breadcrumb;
