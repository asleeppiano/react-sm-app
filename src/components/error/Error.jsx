import React, {Component} from "react"
import style from "./Error.module.css"

class Error extends Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div className={style.Error}>
        <span>{this.props.message}</span>
      </div>
    )
  }
}

export default Error
