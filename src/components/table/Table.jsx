import React, {Component} from "react";
import style from "./Table.module.css";

function isInt(n) {
   return n % 1 === 0;
}

function checkFloat(value) {
    let parsed = Number.parseFloat(value);
    return (!Number.isNaN(parsed)) && (!Number.isInteger(parsed))
}
function checkInt(value){
    let parsed = Number.parseInt(value);
    return (!Number.isNaN(parsed)) && (Number.isInteger(parsed))
}

class TableHeader extends Component{
  constructor(props){
    super(props)
    this.sort = this.sort.bind(this)
    this.state = {
      num: this.props.num,
      sorted: false,
      selected: false,
    }
  }
  sort(e){
    let arr = []
    let dataObj = {}
    const table = this.props.table
    for(let i = 1; i < table.rows.length; i++){
      for(let j = 0; j < table.rows[i].cells.length; j++){
        let val = table.rows[i].cells[j].innerText
        if(checkFloat(val)){
          val = Number.parseFloat(val)
        }else if(checkInt(val)){
          val = Number.parseInt(val)
        }
        dataObj[Object.keys(this.props.data)[j]] = val
      }
      arr.push(Object.assign({}, dataObj))
    }
    if(typeof arr[0][e.target.innerText] === 'number' && this.state.sorted){
      arr.sort((a,b)=>a[e.target.innerText] - b[e.target.innerText])
    }else if(typeof arr[0][e.target.innerText] === 'number' && !this.state.sorted){
      arr.sort((a,b)=>b[e.target.innerText] - a[e.target.innerText])
    }else if(this.state.sorted){
      arr.sort((a, b)=>{
        if(a[e.target.innerText] > b[e.target.innerText]){
          return 1
        }
        if(a[e.target.innerText] < b[e.target.innerText]){
          return -1
        }
        return 0
      })
    }
    else{
      arr.sort((a, b)=>{
        if(a[e.target.innerText] > b[e.target.innerText]){
          return -1
        }
        if(a[e.target.innerText] < b[e.target.innerText]){
          return 1
        }
        return 0
      })
    }
    this.setState((prevState)=>{
      return {sorted: prevState.sorted ? false : true, selected: true, selectedNumber: this.state.num}
    })
    this.props.changeTableState(arr, this.state.num)
  }
  render(){
    return <th num={this.props.num} className={this.props.style} onClick={this.sort}>{this.props.header}</th>
  }
}
class Table extends Component{
  constructor(props){
    super(props)
    this.tableRef = React.createRef()
    this.changeTableState = this.changeTableState.bind(this)
    this.state = {
      data: this.props.data,
      sorted: false,
      selectedNumber: -1,
    }
  }
  changeTableState(arr, num){
    this.setState({data: arr, selectedNumber: num})
  }
  render(){
    if(this.tableRef.current === null){
      this.forceUpdate()
    }
    return(
      <div className={style.Table}>
        <table ref={this.tableRef} className={style.TableContainer}>
          <caption> {this.props.caption} </caption>
          <thead>
            <tr>
              {Object.keys(this.props.data[0]).map((item,index) => {
                if(this.state.selectedNumber === index){
                  return <TableHeader num={index} style={style.TableHeaderSelected} data={this.state.data[0]} table={this.tableRef.current} changeTableState={this.changeTableState} header={item} key={index}/>
                }
                return <TableHeader num={index} style={style.TableHeader} data={this.state.data[0]} table={this.tableRef.current} changeTableState={this.changeTableState} header={item} key={index}/>
              })}
            </tr>
          </thead>
          <tbody>
            {this.state.data.map((item, index) =>(
              <tr key={index}>
                {Object.entries(item).map(([key, value], index) =>(
                  typeof value === "object" ?
                    (
                      <td key={index}>
                        {Object.entries(value).map((x) => x[1]).join(':')}
                      </td>
                    ) : typeof value === 'number' && !Number.isInteger(value) ?
                    <td key={index}> {value.toFixed(4)}</td>:
                    <td key={index}>{value}</td>
                ))
                }
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
