import React, {Component} from "react";
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom'
import {isAuthenticated} from '../utils/Auth'


function PrivateRoute({component: Component, ...rest}){
  return(
    <Route
      {...rest}
      render={props => 
          isAuthenticated() ?
            ( <Component {...props}/> ) :
            ( <Redirect to={{
              pathname: '/login',
              state: {from: props.location}
            }}
          />
      )}
    />
  )
}

export default PrivateRoute;
