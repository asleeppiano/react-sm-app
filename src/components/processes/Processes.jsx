import React, {Component} from "react";
import style from "./Processes.module.css";
import Table from "../table/Table"
import Loading from "../loading/Loading"
import {AppContext} from '../../context/app-context'

class Processes extends Component{
  constructor(props){
    super(props)
    this.state = {
      data: {},
    }
  }
  componentDidMount(){
    this.socket = new WebSocket("ws://89-108-64-92.ovz.vps.regruhosting.ru/api/processes")
    this.socket.onmessage = (e) =>{
      this.setState({data: JSON.parse(e.data)})
    }
  }
  componentWillUnmount(){
    this.socket.close()
  }
  render(){
    let table = <Loading />
    if(Object.keys(this.state.data).length !== 0){
      table = <Table caption="top" data={this.state.data} />
    }
    return(
      <div className={style.Processes}>
        {table}
      </div>
    );
  }
}
Processes.contextType = AppContext

export default Processes;
