import React, {Component} from "react";
import style from "./SystemOverview.module.css";
import CPUCard from "../card/CPUCard.jsx"
import MemCard from "../card/MemCard.jsx"
import NetCard from "../card/NetCard.jsx"
import {AppContext} from '../../context/app-context'

class SystemOverview extends Component{
  constructor(props){
    super(props)
    this.state = {
      data: {
        cores: [],
        net: [],
        mem: [],
      },
      serverinfo: {},
    }
  }
  componentDidMount(){
    this.socket = new WebSocket("ws://89-108-64-92.ovz.vps.regruhosting.ru/api/main")
    this.socket.onmessage = (e) => {
      this.setState({data: JSON.parse(e.data)})
    }
    fetch("/api/serverinfo?" + this.context.guest)
      .then((res) => 
        res.json()
      )
      .then((myJson) =>{
        this.setState({serverinfo: myJson })
      })
      .catch((err) => {
        console.log('so err', err)
      })
  }
  componentWillUnmount(){
    this.socket.close()
  }
  render(){
    let cards = (
      <div className={style.CardsContainer}>
        <CPUCard dataset={this.state.data.cores} header="CPUs History"/>
        <MemCard dataset={this.state.data.mem} header="Memory History"/>
        <NetCard dataset={this.state.data.net} header="Network History"/>
      </div>
    )
    return(
      <div className={style.SystemOverview}>
        <header>
          <h2> Server Info </h2>
          <p> OS: {this.state.serverinfo.os}</p>
          <p> host: {this.state.serverinfo.hostname}</p>
          <p> Uptime: {this.state.serverinfo.uptime}</p>
          <p> CPU: {this.state.serverinfo.cpu}</p>
          <p> GPU: {this.state.serverinfo.gpu}</p>
          <p> Memory: {this.state.serverinfo.mem}</p>
        </header>
        {cards}
      </div>
    );
  }
}
SystemOverview.contextType = AppContext

export default SystemOverview;
