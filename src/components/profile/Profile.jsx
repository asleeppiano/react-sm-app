import React, {Component} from "react";
import style from "./Profile.module.css";
import {AppContext} from '../../context/app-context'
import Image from '../../images/profile.png'
import L from 'leaflet'
import Error from '../error/Error'

class Profile extends Component{
  constructor(props){
    super(props)
    this.mapRef = React.createRef()
    this.getLocation = this.getLocation.bind(this)
    this.state = {
      long: 0,
      lat: 0,
      zoom: 13,
      showLocation: 'none',
    }
    this.map = {}
  }
  componentDidUpdate(){
    if(this.state.showLocation === 'show'){
      let coordinates = [this.state.lat, this.state.long]
      this.map = L.map(this.mapRef.current).setView(coordinates, this.state.zoom)
      L.tileLayer(`https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}`, {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZGVhZGJsYXNwaGVtZXIiLCJhIjoiY2p1anJza2xjMHI4ajRlbmJoem42NGp0cSJ9.WFUFo0X54nPnAi8F-u6evw'
      }).addTo(this.map)
      const marker = L.marker(coordinates).addTo(this.map)
    }
  }
  getLocation(){
    if("geolocation" in navigator && navigator.geolocation){
      navigator.geolocation.getCurrentPosition((position)=>{
        this.setState({long: position.coords.longitude, lat: position.coords.latitude}, ()=>{
          this.setState({showLocation: 'show'})
        })
      }, ()=>{
        this.setState({showLocation: 'error'})
      })
    }else{
      this.setState({showLocation: 'notsupported'})
    }
  }
  render(){
    let getLocationButton = <button className={style.Button} onClick={this.getLocation}>get my location </button>
    let location = ''
    if(this.state.showLocation === 'show'){
      location = <div className={style.Map} ref={this.mapRef}></div>
      getLocationButton = ''
    }else if(this.state.showLocation === 'error'){
      location = <Error message="can't show location"/>
    }else if(this.state.showLocation === 'notsupported'){
      location = <Error message="Geolocation is not supported"/>
    }
    let today = new Date()
    return(
      <div className={style.Profile}>
        <AppContext.Consumer>
          {({user, lastSigned})=>(
            <div className={style.ProfileContainer}>
              <div className={style.ImageContainer}>
                <img width="250" height="250" src={Image} alt="profile pic"/>
              </div>
              <div className={style.ProfileInfoContainer}>
                <h2>user name </h2>
                <span> {user} </span>
                <span> {lastSigned} </span>
                <h2> current time </h2>
                <span> {today.toString()}  </span>
                <h2> current location </h2>
                {getLocationButton}
                {location}
              </div>
            </div>
          )}
        </AppContext.Consumer>
      </div>
    );
  }
}

export default Profile;
