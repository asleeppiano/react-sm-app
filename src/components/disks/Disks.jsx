import React, {Component} from "react";
import style from "./Disks.module.css";
import DiskCard from "./DiskCard"

class Disks extends Component{
  render(){
    return (
      <section className={style.Disks}>
        <DiskCard path="/home/admin/go" />
        <DiskCard path="/home/admin/folder" />
        <DiskCard path="/home/admin/boom" />
      </section>
    )
  }
}

export default Disks
