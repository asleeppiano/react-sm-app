import React, {Component} from "react";
import style from "./DiskCard.module.css";
import {Doughnut} from 'react-chartjs-2'
import Breadcrumb from '../breadcrumb/Breadcrumb.jsx'
import axios from 'axios'
import Loading from '../loading/Loading'
import {AppContext} from '../../context/app-context'

let state = []

class DiskCard extends Component{
  constructor(props){
    super(props)
    if(state.length === 0){
      this.state = {
        path: this.props.path,
        diskstat: [],
        dataset: {
          labels: [],
          datasets:[{
            data: [],
            backgroundColor: []
          }]
        },
        download: false,
      }
    }else{
      this.state = state.shift()
    }
  }
  colorGenerator(){
    let opacity = 0.9
    let colors = [
      `rgba(255,99,132)`,
      `rgba(117, 221, 82)`,
      `rgba(54,162,235)`,
      `rgba(255, 159, 64)`,
      `rgba(231, 250, 93)`,
      `rgba(236, 138, 231)`,
      `rgba(153, 102, 255)`,
      `rgba(75, 192, 192)`,
      `rgba(22, 161, 52)`,
      `rgba(255,206,86)`,
      `rgba(84,84,241)`,
      `rgba(129, 26, 220)`,
      `rgba(180, 18, 18)`,
      `rgba(255, 255, 255)`,
      `rgba(25, 27, 25)`,
    ]
    if(this.state.diskstat.length < 16){
      return colors
    }
    let j = 1
    let k = 0
    for(let i = 0; i < this.state.diskstat.length - 15; i++){
      if(i*j < 15 && opacity > 0.4){
        opacity -= 0.1
      }else{
        j++
      }
      if(k > 15){
        k = 0
      }
      colors.push(colors[k].substring(0, colors[k].length-1) + `,${opacity})`)
      k++
    }
    return colors
  }
  diskStatRequest(){
    axios.get('/api/disk/stat?path=' + this.state.path + "&" + this.context.guest, {
      onDownloadProgress: this.onDownload(),
      cancelToken: this.source.token,
    }).then((res)=> res.data)
      .then((myJson)=>{
        this.setState({diskstat: myJson, download: false}, () => {
          let labelList = []
          let dataList = []
          for(let file of this.state.diskstat){
            labelList.push(file.path)
            dataList.push(Math.floor(file.diskusage / 1048576))
          }
          this.setState({dataset: {
            labels: labelList,
            datasets:[{
              data: dataList, 
              backgroundColor: this.colorGenerator(),
            }]
          },
            path: this.state.path})
        })
      }).catch((err)=>{
        if(axios.isCancel(err)){
          console.log("request canceled", err.message)
        }else if (err.response){
          console.log('disk card', err.response.message)
        }
      })
  }
  onDownload(){
    this.setState((prevState) => {
      return {download: prevState.download ? false : true}
    })
  }
  componentDidMount(){
    this.CancelToken = axios.CancelToken
    this.source = this.CancelToken.source()
    if(this.state.diskstat.length === 0)
      this.diskStatRequest()
  }
  componentDidUpdate(prevProps, prevState){
    if(prevState.path !== this.state.path){
      this.CancelToken = axios.CancelToken
      this.source = this.CancelToken.source()
      this.diskStatRequest()
    }
  }
  componentWillUnmount(){
    if(this.state.download === true){
      this.source.cancel('Operation canceled by user')
      return
    }
    state.push(this.state)
  }
  onPathChange(val){
    console.log('val', val)
    // this.setState({path: val[0] +  val.slice(1, val.length).join('/')})
    if(val.length !== 1){
      this.setState({path: val.join('/')})
      return
    }
    this.setState({path: "/"})
  }
  render(){
    let diskPath = [""]
    let dset = this.state.dataset
    console.log(this.state.diskstat)
    if(this.state.diskstat.length !== 0){
      if(this.state.path !== "/"){
        diskPath = this.state.path.split('/')
      }
    }
    let card = (
      <div>
        <Doughnut data={dset} options={{legend:{position: 'left'}}} />
      </div>
    )
    if(this.state.download){
      card = (
        <Loading />
      )
    }
    return(
        <div className={style.DiskCard}>
          <h2> Disk usage </h2>
          <Breadcrumb onPathChange={this.onPathChange.bind(this)} path={diskPath}/>
          {card}
        </div>
    );
  }
}

DiskCard.contextType = AppContext

export default DiskCard;
