import React, {Component} from "react";
import style from "./User.module.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import {AppContext} from '../../context/app-context'

class User extends Component{
  render(){
    let userStyle = style.User
    if(this.props.preventClick === true){
      userStyle = `${style.User} ${style.PreventClick}`
    }
    return(
      <AppContext.Consumer>
        {({user})=> (
          <div className={userStyle}>
            <Link onClick={this.props.setSectionName} to="/profile">
              <span> {user} </span>
              <div className={style.UserCircle}>
                <span>{user.length !== 0 ? user[0].toUpperCase(): <React.Fragment/>}</span>
              </div>
            </Link>
          </div>
        )}
      </AppContext.Consumer>
    );
  }
}

export default User;
