import axios from 'axios'

let authenticated = false
let currentUser = ''

export function isAuthenticated(){
  return authenticated
}

export function getUser(){
  return currentUser
}

export function loginAsGuest(){
  authenticated = true
  currentUser = 'Guest'
}

export async function register(name, password){
  const user = {
    name: name,
    password: password,
  }
  return await axios.post('/api/register', user)
    .then(function(response){
      authenticated = true
      currentUser = user.name
      return currentUser
    })
    .catch(function(err){
      if(err.response){
        console.log(err.response.data)
        console.log(err.response.status)
        console.log(err.response.headers)
      }
      console.log(err.message)
      authenticated = false
      return err.response.data
    })
}

export async function login(name, password){
  const user = {
    name: name,
    password: password,
  }
  return await axios.post('/api/login',user,
    {
      headers: {
        'Content-Type': 'application/json',
        'Allow': 'application/json, text/plain'
      },
    }).then(function(response){
      authenticated = true
      currentUser = user.name
      return currentUser
    }).catch(function(err){
      if(err.response){
        console.log(err.response.data)
        console.log(err.response.status)
        console.log(err.response.headers)
      }
      console.log(err.message)
      authenticated = false
      return err.response.data
    })
}

export async function refresh(){
  return await axios.post('/api/refresh')
    .then(function(response){
      authenticated = true
    })
    .catch(function(err){
      console.log(err)
    })
}

