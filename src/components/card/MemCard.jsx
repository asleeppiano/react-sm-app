import React, {Component} from "react";
import style from "./CPUCard.module.css";
import {Line} from "react-chartjs-2"
import CardWrapper from '../../HOC/CardWrapper.jsx'
import 'chartjs-plugin-streaming'
import Loading from '../loading/Loading'


class MemCard extends Component{

  constructor(props){
    super(props)
    this.state = {
      data: {
        datasets: [] 
      },
      opts: {},
    }
    this.onRefresh = (chart) => {
      chart.config.data.datasets[0].data.push({
          x: Date.now(),
          y: this.props.dataset.virt,
        });
      chart.config.data.datasets[1].data.push({
          x: Date.now(),
          y: this.props.dataset.swap,
        });
    }
    this.line = ''
    this.flag = true
  }

  static getDerivedStateFromProps(props, state){
    if (props.dataset.length !== 0 && state.data.datasets.length === 0){
      let colors = {
        backgroundColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
      }
      let datasetlabels = new Array(2)
      const memLabels = ['virtual', 'swap']
      for(let i = 0; i < datasetlabels.length; i++){
        datasetlabels[i] = {
          label: memLabels[i],
          backgroundColor: colors.backgroundColor[i],
          borderColor: colors.borderColor[i],
          fill: false,
          cubicInterpolationMode: 'monotone',
          data: [],
        }
      }
      return {
        data: {
          datasets: datasetlabels
        },
      }
    } 
    return null
  }

  componentDidMount(){
    if (Object.keys(this.state.opts).length === 0){
      this.props.wrappedstate.opts.scales.xAxes[0].realtime.onRefresh = this.onRefresh
      const newOpts = this.props.wrappedstate.opts
      this.setState({opts: newOpts}, () => {
      })
    }
  }
  render(){
      // console.log(this.props.dataset)
    if (this.state.data.datasets.length !== 0 && this.flag && Object.keys(this.state.opts).length !== 0){ 
      this.line = <Line data={this.state.data} options={this.state.opts}/>
      this.flag = false
    }else if (this.flag){
      this.line =( 
        <Loading />
      )
    }
    return(
      <div className={style.Card}>
        <h1> {this.props.header} </h1>
        <div className={style.ChartContainer}>
          {this.line}
        </div>
      </div>
    );
  }
}

export default CardWrapper(MemCard);
