import React, { Component } from 'react';
import './App.css';
import Layout from "./components/layout/Layout.jsx"
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import SystemOverview from "./components/systemoverview/SystemOverview.jsx"
import Network from "./components/network/Network.jsx"
import Disks from "./components/disks/Disks.jsx"
import Login from "./components/login/Login"
import PrivateRoute from "./components/privateroute/PrivateRoute"
import * as Auth from "./components/utils/Auth"
import {AppContext} from './context/app-context'
import Profile from './components/profile/Profile'
import Processes from './components/processes/Processes'

class App extends Component {
  constructor(props){
    super(props)
    this.loadLayout = this.loadLayout.bind(this)
    this.setUser = this.setUser.bind(this)
    this.state = {
      load: false,
      user: '',
      setUser: this.setUser,
      guest: '',
    }
  }
  loadLayout(){
    if(Auth.isAuthenticated()){
      this.setState({load: true})
    }
  }
  setUser(user){
    this.setState({user: user})
  }
  render() {
    return (
      <Router>
        <AppContext.Provider value={this.state}>
          <Layout load={this.state.load}>
            <PrivateRoute exact path="/" component={SystemOverview} />
            <PrivateRoute path="/profile" component={Profile} />
            <PrivateRoute path="/network" component={Network} />
            <PrivateRoute path="/disks" component={Disks} />
            <PrivateRoute path="/processes" component={Processes} />
          </Layout>
          <Route path="/login" render={props => <Login loadLayout={this.loadLayout} {...props}/>} />
        </AppContext.Provider>
      </Router>
    );
  }
}

export default App;
