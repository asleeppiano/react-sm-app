/* Top Disk Usage.
 * Copyright (C) 2019 Joseph Paul <joseph.paul1@gmx.com>
 * https://bitbucket.org/josephpaul0/tdu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* This program estimates the disk usage of all files in a given path and then
 * displays a sorted list of the biggest items.  The estimation method used is
 * similar to the 'du -skx' command from GNU Coreutils package.
 */

package tdu

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
    "syscall"
    "errors"
    "sort"
    "encoding/json"
)

type File struct { // File information for each scanned item
    Path       string `json:"path"`
    Fullpath   string `json:"fullpath"`
    Name       string `json:"name"`
    IsRegular  bool `json:"isregular"`
    IsDir      bool `json:"isdir"`
    IsSymlink  bool `json:"issymlink"`
    IsOtherFs  bool `json:"isotherfs"`
    IsSpecial  bool `json:"isspecial"`
    ReadError  bool `json:"readerror"`
    Size       int64 `json:"size"`
    DiskUsage  int64 `json:"diskusage"`
    Depth      int64 `json:"depth"`
    Items      int64 `json:"items"`
    BlockSize  int64 `json:"blocksize"`
    NBlocks512 int64 `json:"nblocks512"`
    Inode      uint64 `json:"inode"`
    NLinks     uint64 `json:"nlinks"`
    DeviceId   uint64 `json:"deviceid"`
    fi         os.FileInfo
}

type ino_map map[uint64]uint16 // map of inode number and counter

type s_scan struct { // Global variables
	nErrors       int64    // number of Lstat errors
	nDenied       int64    // number of access denied
	nItems        int64    // number of scanned items
	nFiles        int64    // number of files
	nDirs         int64    // number of directories
	nSymlinks     int64    // number of symlinks
	nHardlinks    int64    // number of hardlinks
	maxNameLen    int64    // max filename length for depth = 1
	nSockets      int64    // number of sockets
	nCharDevices  int64    // number of character devices
	nBlockDevices int64    // number of block devices
	maxWidth      int64    // display width (tty columns)
	reachedDepth  int64    // maximum directory depth reached
	maxPathLen    int64    // maximum directory path length
	maxFNameLen   int64    // maximum filename length
	currentDevice uint64   // device number of current partition
	maxShownLines int      // number of depth 1 items to display
	maxBigFiles   int      // number of biggest files to display
	wsl           bool     // Windows Subsystem for Linux
	partinfo      bool     // found info about partition
	foundBoundary bool     // found other filesystems
	hideMax       bool     // hide deepest and longest paths
	export        bool     // export result to Ncdu's JSON format
	exportPath    string   // path to exported file
	exportFile    *os.File // exported file
	deepestPath   string   // deepest subdirectory reached
	longestPath   string   // longest directory path
	longestFName  string   // longest filename
	os            string   // operating system
	fsType        string   // FS type from /proc/mounts
	partition     string   // current partition
	mountOptions  string   // mount options from /proc/mounts
	pathSeparator string   // os.PathSeparator as string
	inodes        ino_map  // inode number to file path
	bigfiles      []File
	start         time.Time // time at process start
}

func newScanStruct(start time.Time) *s_scan {
	var sc s_scan
	sc.pathSeparator = string(os.PathSeparator)
	sc.inodes = make(map[uint64]uint16, 256)
	sc.start = start
	return &sc
}

type szDesc []File

func (a szDesc) Len() int           { return len(a) }
func (a szDesc) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a szDesc) Less(i, j int) bool { return a[i].DiskUsage > a[j].DiskUsage }


// Fallback to approximate disk usage
func avgDiskUsage(sz, bsize int64) int64 {
	if sz == 0 {
		return 0
	}
	if sz < bsize {
		return bsize
	}
	d := sz / bsize
	i := d * bsize
	if sz > i {
		return i + bsize
	}
	return sz
}

func sysStat(sc *s_scan, f *File) error {
	sys := f.fi.Sys()
	if sys == nil {
		panic("Stat System Interface Not Available ! Linux is required")
	}
	stat, ok := sys.(*syscall.Stat_t)
	if !ok {
        fmt.Printf("ok: %v\n", ok)
        fmt.Printf("%v,  %T\n", stat, stat)
        return errors.New("cant convert sys to syscall.Stat_t")
	}
	f.DeviceId = stat.Dev
	f.Inode = stat.Ino
	f.NLinks = stat.Nlink
	f.BlockSize = stat.Blksize
	f.NBlocks512 = stat.Blocks
	f.DiskUsage = 512 * f.NBlocks512
	if f.Depth == 1 {
		sc.currentDevice = f.DeviceId
	}
	if f.DeviceId != sc.currentDevice {
		f.IsOtherFs = true
		sc.foundBoundary = true
	}
	_, ok = sc.inodes[f.Inode]
	if ok { // Hardlink means inode used more than once in map
		if !f.IsOtherFs { // Other FS may have a same inode number (root=2)
			f.DiskUsage = 0
			sc.nHardlinks++
		}
	}
	// Each occurrence of inode is counted
	sc.inodes[f.Inode]++
	return nil
}

func fullStat(sc *s_scan, path string, depth int64) (*File, error) {
	fi, err := os.Lstat(path)
	if err != nil {
		sc.nErrors++
		// fmt.Println(err)
		return nil, err
	}
	sc.nItems++
	wd, _ := os.Getwd()
	var fullPath string
	if wd == "/" {
		fullPath = wd + path
	} else {
		fullPath = wd + sc.pathSeparator + path
	}
	f := File{Path: path, Fullpath: fullPath, Name: fi.Name(), Depth: depth,
		Size: fi.Size(), IsDir: fi.IsDir(), BlockSize: 4096, fi: fi}
	// Firstly, disk usage is estimated with a block size of 4kb,
	// then it will be precisely calculated with a native syscall.
	f.DiskUsage = avgDiskUsage(f.Size, f.BlockSize)

	l := int64(len(fullPath))
	if f.IsDir && l > sc.maxPathLen {
		sc.maxPathLen = l
		sc.longestPath = fullPath
	}
	l = int64(len(f.Name))
	if l > sc.maxFNameLen {
		sc.maxFNameLen = l
		sc.longestFName = f.Name
	}
	if depth > sc.reachedDepth {
		sc.reachedDepth = depth
		sc.deepestPath = filepath.Dir(f.Fullpath)
	}
	switch mode := fi.Mode(); {
	case mode.IsRegular():
		f.IsRegular = true
		sc.nFiles++

	case mode.IsDir():
		sc.nDirs++

	case mode&os.ModeSymlink != 0: // True if the file is a symlink.
		sc.nSymlinks++
		f.IsSymlink = true
		if f.Size < 60 { // On Linux, fast symlinks are stored in inode
			f.DiskUsage = 0
		}

	case mode&os.ModeNamedPipe != 0:
		//fmt.Printf("  Named pipe: [%s]\n", f.fullpath)
		f.IsSpecial = true

	case mode&os.ModeCharDevice != 0:
		//fmt.Printf("  Character Device: [%s]\n", f.fullpath)
		sc.nCharDevices++
		f.IsSpecial = true

	case mode&os.ModeDevice != 0:
		//fmt.Printf("  Block device: [%s]\n", f.fullpath)
		sc.nBlockDevices++
		f.IsSpecial = true

	case mode&os.ModeSocket != 0:
		//fmt.Printf("  Socket: [%s]\n", f.fullpath)
		sc.nSockets++
		f.IsSpecial = true

	default:
		fmt.Printf("  Unknown file type (%v): [%s]\n", mode, f.Fullpath)
	}
	err = sysStat(sc, &f)
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}
	return &f, nil
}

func scan(sc *s_scan, files *[]File, path string, depth int64) (*File, error) {
	f, err := fullStat(sc, path, depth)
	if err != nil {
		// fmt.Println(err)
		return nil, err
	}

	if f.IsOtherFs {
		return f, nil
	}
	if f.IsSymlink || !f.IsDir {
		if files != nil {
			*files = append(*files, *f)
		}
		if len(sc.bigfiles) > sc.maxBigFiles*4 {
			sort.Sort(szDesc(sc.bigfiles))
			sc.bigfiles = sc.bigfiles[0:sc.maxBigFiles]
		}
		sc.bigfiles = append(sc.bigfiles, *f)
		return f, nil
	}

	fs, err := ioutil.ReadDir(path)
	if err != nil {
		sc.nDenied++
		f.ReadError = true
		// fmt.Printf("ReadDir err on \"%s\", len(fs)=%d\n", path, len(fs))
	}

	var size, du, items int64 = f.Size, f.DiskUsage, 0
	var ptr *[]File
	for _, i := range fs { // Calculate total size by recursive scanning
		ptr = files
		if depth > 1 {
			ptr = nil // Forget details for deep directories
		}
		items++
		var subpath string
		if path == "." {
			subpath = i.Name()
		} else {
			subpath = path + sc.pathSeparator + i.Name()
		}
		cf, err := scan(sc, ptr, subpath, depth+1)
		if err != nil {
			//fmt.Println(err)
			continue
		}
		size += cf.Size
		du += cf.DiskUsage
		items += cf.Items
	}
	fo := File{Path: path, Name: f.Name, Size: size, DiskUsage: du,
		IsDir: true, Depth: depth, Items: items}
	if depth > 1 && files != nil {
		*files = append(*files, fo)
	}
	return &fo, nil
}


func showElapsed(sc *s_scan) {
	elapsed := time.Since(sc.start)
	fmt.Printf("\n  Total time: %.3f s\n\n", elapsed.Seconds())
}

/*Tdu Basically, the process has got several steps:
 * 1. change directory to given path
 * 2. scan all files recursively, collecting 'stat' data
 */
func Tdu(path string) ([]byte, error) {
	start := time.Now()
	sc := newScanStruct(start)
    f, err := os.Lstat(path)
    if err != nil {
        return nil, err
    }
    if !f.IsDir(){
        return nil, errors.New("path should be a dir")
    }
    if err := os.Chdir(path); err != nil{
        return nil, err
    }
	var fi []File
	scan(sc, &fi, ".", 1)
    b, err := json.Marshal(fi)
    if err != nil{
        return nil, err
    }
	showElapsed(sc)
    return b, nil
}
