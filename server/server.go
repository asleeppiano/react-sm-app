package main

import (
    "sync"
    "encoding/json"
    _"fmt"
    "github.com/gorilla/websocket"
    "github.com/shirou/gopsutil/cpu"
    "github.com/shirou/gopsutil/host"
    "github.com/shirou/gopsutil/mem"
    "github.com/shirou/gopsutil/net"
    "github.com/shirou/gopsutil/process"
    "github.com/dgrijalva/jwt-go"
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
    "./tdu"
    "log"
    "net/http"
    "os/exec"
    "time"
    "strconv"
    _ "encoding/binary"
    _ "bytes"
    "io/ioutil"
    "golang.org/x/crypto/bcrypt"
)

type Command struct {
    name string
    args []string
}
type CommandList []Command

type ServerInfo struct {
    Os       string `json:"os"`
    Hostname string `json:"hostname"`
    Uptime   string `json:"uptime"`
    CPU      string `json:"cpu"`
    Gpu      string `json:"gpu"`
    Mem      string `json:"mem"`
}

type CompHist struct{
    Cores []float64 `json:"cores"`
    Mem MemStat `json:"mem"`
    Net NetStat `json:"net"`
}

type MemStat struct{
    Virt float64 `json:"virt"`
    Swap float64 `json:"swap"`
}

type NetStat struct{
    BytesSent   uint64 `json:"bytesSent"`
    BytesRecv   uint64 `json:"bytesRecv"`
}

type User struct{
    Name string `json:"name"`
    Password string `json:"password"`
}

type Claims struct {
    Username string `json:"name"`
    jwt.StandardClaims
}

var db *sql.DB

var jwtKey = []byte("super_secret_key")

func parsePipedCommands(comms ...*exec.Cmd) []byte {
    for i, c := range comms[:len(comms)-1] {
        pipe, err := c.StdoutPipe()
        if err != nil {
            log.Println("stdoutpipe ", err)
            return []byte("cannot get gpu info")
        }
        comms[i+1].Stdin = pipe
        if err := c.Start(); err != nil {
            log.Println("cmd start ", err)
            return []byte("cannot get gpu info")
        }
    }
    res, err := comms[len(comms)-1].Output()
    if err != nil {
        log.Println("comm last output ", err)
        return []byte("cannot get gpu info")
    }
    return res
}

var upgrader = websocket.Upgrader{
    ReadBufferSize:  1024,
    WriteBufferSize: 1024,
    CheckOrigin: func(r *http.Request) bool {
        return true
    },
}

func gpuInfo() []byte {
    lspci := exec.Command("lspci")
    grep := exec.Command("grep", "VGA")
    awk := exec.Command("awk", "-F:", "{print $3;}")
    gpuInfo := parsePipedCommands(lspci, grep, awk)
    return gpuInfo
}

func ComputerHistory() ([]byte, error) {
    cpuStat, err := cpu.Percent(time.Second, true)
    if err != nil {
        log.Println("cpu percent ", err)
        return nil, err
    }
    memStatVirt, err := mem.VirtualMemory()
    if err != nil {
        log.Println("mem stat ", err)
        return nil, err
    }
    memStatSwap, err := mem.SwapMemory()
    if err != nil {
        log.Println("mem stat ", err)
        return nil, err
    }
    netStat, err := net.IOCounters(false)
    if err != nil {
        log.Println("net stat ", err)
        return nil, err
    }
    res := CompHist{
        Cores: cpuStat,
        Mem: MemStat{
            Virt: memStatVirt.UsedPercent,
            Swap: memStatSwap.UsedPercent,
        },
        Net: NetStat{
            BytesRecv: netStat[0].BytesRecv,
            BytesSent: netStat[0].BytesSent,
        },
    }
    data, err := json.Marshal(res)
    if err != nil {
        log.Println("percent marshal ", err)
        return nil, err
    }
    return data, err
}

func NetworkStat() ([]byte, error) {
    connStat, err := net.Connections("inet")
    if err != nil {
        log.Println("connStat err ", err)
        return nil, err
    }
    b, err := json.Marshal(connStat)
    if err != nil {
        log.Println("connStat marshal ", err)
        return nil, err
    }
    return b, err
}

func DiskStat(path string) ([]byte, error) {
    b, err := tdu.Tdu(path)
    if err != nil {
        log.Println("disk usage ", err.Error())
        return nil, err
    }
    return b, err
}


func serverInfoHandler(w http.ResponseWriter, r *http.Request) {
    if r.Method == http.MethodGet {
        osInfo, err := host.Info()
        if err != nil {
            log.Println("host info ", err)
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        osInfoRes := osInfo.Platform
        hostRes := osInfo.Hostname
        upTimeRes, err := time.ParseDuration(strconv.Itoa(int(osInfo.Uptime)) + "s")
        if err != nil {
            log.Println("uptime ", err)
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        cpuInfo, err := cpu.Info()
        if err != nil {
            log.Println("cpu info ", err)
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        cpuInfoRes := cpuInfo[0].ModelName
        gpuInfoRes := gpuInfo()
        memInfo, err := mem.VirtualMemory()
        memInfoTotalRes := int(memInfo.Total)
        memInfoUsedRes := int(memInfo.Used)
        memInfoRes := strconv.Itoa(memInfoUsedRes / 1024 / 1024) + "/" +
        strconv.Itoa(memInfoTotalRes / 1024 / 1024)
        serInfo := ServerInfo{
            Os:       osInfoRes,
            Hostname: hostRes,
            Uptime:   upTimeRes.String(),
            CPU:      cpuInfoRes,
            Gpu:      string(gpuInfoRes),
            Mem:      memInfoRes,
        }
        out, err := json.Marshal(serInfo)
        if err != nil {
            log.Println("json marshal ", err)
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        w.WriteHeader(http.StatusOK)
        w.Write(out)
    }
}

func netConnStatHandler(w http.ResponseWriter, r *http.Request) {
    if r.Method == http.MethodGet {
        out, err := NetworkStat()
        if err != nil{
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        w.WriteHeader(http.StatusOK)
        w.Write(out)
    }
}

var mutex = &sync.Mutex{}
func diskStatHandler(w http.ResponseWriter, r *http.Request) {
    if r.Method == http.MethodGet {
        path, ok := r.URL.Query()["path"]
        if !ok || len(path[0]) < 1 {
            log.Println("Url Param 'key' is missing")
            return
        }
        mutex.Lock()
        out, err := DiskStat(path[0])
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        mutex.Unlock()
        w.WriteHeader(http.StatusOK)
        w.Write(out)
    }
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
    conn, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Println("upgrader", err)
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    for {
        res, err := ComputerHistory()
        if err != nil{
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        if err := conn.WriteMessage(websocket.TextMessage, res); err != nil {
            log.Println("write message:",err.Error())
            if err := conn.Close(); err != nil{
                log.Println("conn close:",err.Error())
            }
            return
        }
        time.Sleep(time.Second)
    }
}


func registerHandler(w http.ResponseWriter, r *http.Request){
    if r.Method == http.MethodPost {
        var userFromClient User
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }
        if err := json.Unmarshal(body, &userFromClient); err != nil{
            http.Error(w, "cannot unmarshal json " + err.Error(), http.StatusInternalServerError)
            return
        }
        hash, err := bcrypt.GenerateFromPassword([]byte(userFromClient.Password), bcrypt.DefaultCost)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        _, err = db.Exec("insert into users(name,password) values(?,?)", userFromClient.Name, hash)
        if err != nil{
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        expirationTime := time.Now().Add(time.Hour)
        claims := Claims{
            Username: userFromClient.Name,
            StandardClaims: jwt.StandardClaims{
                ExpiresAt: expirationTime.Unix(),
            },
        }

        token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

        tokenString, err := token.SignedString(jwtKey)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        http.SetCookie(w, &http.Cookie{
            Name: "token",
            Value: tokenString,
            Expires: expirationTime,
        })
        w.Write([]byte("user registered"))
    }
}

type Process struct {
    Pid int32 `json:"pid"`
    Name string `json:"name"`
    Username string `json:"username"`
    Nice int32 `json:"nice"`
    Virt uint64 `json:"virt"`
    Res uint64 `json:"res"`
    Shr uint64 `json:"shr"`
    Status string `json:"status"`
    Cpupercent float64 `json:"cpupercent"`
    Mempercent float32 `json:"mempercent"`
}

func getProcessesInfo() ([]byte, error){
    processList, err := process.Processes()
    if err != nil{
        log.Println("cant get processes ", err.Error())
    }
    var processes []Process
    for _, v := range processList{
        name, err := v.Name()
        if err != nil{
            log.Println("name:",err.Error())
            return nil, err
        }
        username, err := v.Username()
        if err != nil{
            log.Println("name:",err.Error())
            return nil, err
        }
        nice, err := v.Nice()
        if err != nil{
            log.Println("name:",err.Error())
            return nil, err
        }
        meminfo, err := v.MemoryInfoEx()
        if err != nil{
            log.Println("name:",err.Error())
            return nil, err
        }
        virt := meminfo.VMS
        res := meminfo.RSS
        shr := meminfo.Shared
        status, err := v.Status()
        if err != nil{
            log.Println("name:",err.Error())
            return nil, err
        }
        cpupercent, err := v.CPUPercent()
        if err != nil{
            log.Println("name:",err.Error())
            return nil, err
        }
        mempercent, err := v.MemoryPercent()
        if err != nil{
            log.Println("name:",err.Error())
            return nil, err
        }
        processes = append(processes, Process{
            v.Pid,
            name,
            username,
            nice,
            virt,
            res,
            shr,
            status,
            cpupercent,
            mempercent,
        })
    }
    b, err := json.Marshal(processes)
    return b, err
}

func processesHandler(w http.ResponseWriter, r *http.Request){
    conn, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Println("upgrader", err)
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    for {
        res, err := getProcessesInfo()
        if err != nil{
            continue
        }
        if err := conn.WriteMessage(websocket.TextMessage, res); err != nil {
            log.Println("write message:",err.Error())
            if err := conn.Close(); err != nil{
                log.Println("conn close:",err.Error())
            }
            return
        }
        time.Sleep(time.Second * 2)
    }
}

func loginHandler(w http.ResponseWriter, r *http.Request){
    if r.Method == http.MethodPost{
        var userFromClient User
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }
        if err := json.Unmarshal(body, &userFromClient); err != nil{
            log.Println("cannot unmarshal json", err)
            http.Error(w, "cannot unmarshal json " + err.Error(), http.StatusInternalServerError)
            return
        }
        var user User
        if err := db.QueryRow("select name, password from users where name = ?", userFromClient.Name).Scan(&user.Name, &user.Password); err != nil{
            if err == sql.ErrNoRows {
                // there were no rows, but otherwise no error occurred
                http.Error(w, err.Error(), http.StatusNotFound)
                return
            }
            log.Println(err)
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }

        if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userFromClient.Password)); err != nil{
            http.Error(w, err.Error(), http.StatusForbidden)
            return
        }

        expirationTime := time.Now().Add(time.Hour)
        claims := Claims{
            Username: user.Name,
            StandardClaims: jwt.StandardClaims{
                ExpiresAt: expirationTime.Unix(),
            },
        }

        token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

        tokenString, err := token.SignedString(jwtKey)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
        http.SetCookie(w, &http.Cookie{
            Name: "token",
            Value: tokenString,
            Expires: expirationTime,
        })
        w.Write([]byte("logged in"))
    }
}

func protectedHandler(fn func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc{
    return func(w http.ResponseWriter, r *http.Request){
        path, ok := r.URL.Query()["guest"]
        if ok && path[0] == "true"  {
            fn(w, r)
            return
        }
        c, err := r.Cookie("token")
        if err != nil {
            if err == http.ErrNoCookie{
                http.Error(w, err.Error(), http.StatusUnauthorized)
                log.Println(err.Error())
                return
            }
            http.Error(w, err.Error(), http.StatusBadRequest)
            log.Println(err.Error())
            return
        }
        tknStr := c.Value

        claims := &Claims{}

        tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
            return jwtKey, nil
        })
        if !tkn.Valid {
            http.Error(w, err.Error(), http.StatusUnauthorized)
            return
        }
        if err != nil {
            if err == jwt.ErrSignatureInvalid {
                http.Error(w, err.Error(), http.StatusUnauthorized)
                return
            }
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }
        fn(w,r)
    }
}

func refreshHandler(w http.ResponseWriter, r *http.Request){
    if r.Method == http.MethodPost{
        c, err := r.Cookie("token")
        if err != nil {
            if err == http.ErrNoCookie {
                http.Error(w, err.Error(), http.StatusUnauthorized)
                return
            }
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }
        tknStr := c.Value
        claims := &Claims{}
        tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
            return jwtKey, nil
        })
        if !tkn.Valid {
            http.Error(w, err.Error(), http.StatusUnauthorized)
            return
        }
        if err != nil {
            if err == jwt.ErrSignatureInvalid {
                http.Error(w, err.Error(), http.StatusUnauthorized)
                return
            }
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }

        if time.Unix(claims.ExpiresAt, 0).Sub(time.Now()) > 30*time.Second {
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }

        expirationTime := time.Now().Add(time.Hour)
        claims.ExpiresAt = expirationTime.Unix()
        token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
        tokenString, err := token.SignedString(jwtKey)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }

        http.SetCookie(w, &http.Cookie{
            Name:    "token",
            Value:   tokenString,
            Expires: expirationTime,
        })
    }
}

func main() {
    var err error
    db, err = sql.Open("mysql", "root@/authJwt")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()
    http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir("../build"))))
    http.HandleFunc("/api/main", mainHandler)
    http.HandleFunc("/api/serverinfo", protectedHandler(serverInfoHandler))
    http.HandleFunc("/api/net/connstat", protectedHandler(netConnStatHandler))
    http.HandleFunc("/api/disk/stat", protectedHandler(diskStatHandler))
    http.HandleFunc("/api/processes", processesHandler)
    http.HandleFunc("/api/login", loginHandler)
    http.HandleFunc("/api/refresh", refreshHandler)
    http.HandleFunc("/api/register", registerHandler)
    log.Fatal(http.ListenAndServe(":80", nil))
}
